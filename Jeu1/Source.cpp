#include<iostream>
#include<string>

#include "PiedDeLaColline.h"
#include "gestionCombats.h"
#include "RencontreAvecGobelin.h"

using namespace std;

int main()
{

	string nomHero("");
	int pvHero(20);
	int pvGobelin(10);
	int pvTroll(25);

	int const attHero(20);
	int const attGobelin(5);
	int const attTroll(15);

	int const defHero(15);
	int const defGobelin(5);
	int const defTroll(10);

	int vivantOuMort(0);

	string nomCreature("");

	cout << "L'aventure commence" << endl;
	cout << "La taverne des nez tordus n'est pas votre preferee mais aujourd'hui vous y sirotez paisiblement un lait de chevre" << endl;
	cout << "Autour de vous, l'aubergiste, un ivrogne et trois clients qui jouent aux des en discutant." << endl;
	cout << "Soudain l'ivrogne prend les joueurs a parti." << endl;
	cout << "Au lieu de jouer, vous feriez mieux de chasser le troll qui reside dans la foret, il est dangereux pour les villageois !" << endl;
	cout << "La discussion de comptoir s'engage, mais personne n'a vraiment l'air de vouloir s'attaquer au probleme." << endl;
	cout << "Avec votre epee a la ceinture, qu'allez vous faire ?" << endl;
	cout << "1 - Demander des renseignements sur ce Troll" << endl;
	cout << "2 - Vous rendre directement dans la foret" << endl;

	int premierChoix(0);

	cin >> premierChoix;

	switch (premierChoix)
	{
	case 1:
	{
		cout << "Vous approchez de la table de jeux et demandez : \"alors comme ca un troll rode dans la foret ?\"" << endl;
		cout << "Le plus vieux des joueurs repond : \"il parait, oui. Il serait dans la grotte qui se trouve au pied de la colline." << endl;
		piedDeLaColline();
		nomCreature = "Le Troll";
		vivantOuMort = gestionCombat(pvHero, pvTroll, attHero, attTroll, defHero, defTroll, nomCreature);
		if (vivantOuMort == 1)
		{
			cout << "Vous avez ete tue ! L'aventure se termine ici." << endl;
			return 0;
		}
		break;
	}
	case 2:
		cout << "Vouv vous mettez en marche et penetrez dans la grande foret hostile." << endl;
		rencontreAvecGobelin();
		nomCreature = "Le Gobelin";
		vivantOuMort = gestionCombat(pvHero, pvGobelin, attHero, attGobelin, defHero, defGobelin, nomCreature);
		
		if (vivantOuMort == 1)
		{
			cout << "Vous avez ete tue ! L'aventure se termine ici." << endl;
			return 0;
		}

		cout << "Il vous reste " << pvHero << endl;
		cout << "Vous prenez soin de vos erafflures et recuperez 10 PV." << endl;
		pvHero += 10;
		cout << "Il vous reste " << pvHero << endl;
		cout << "Vous reprenez votre route lorsqu'au loin apparait une petite colline." << endl;
		piedDeLaColline();
		nomCreature = "Le Troll";
		vivantOuMort = gestionCombat(pvHero, pvTroll, attHero, attTroll, defHero, defTroll, nomCreature);
		
		if (vivantOuMort == 1)
		{
			cout << "Vous avez ete tue ! L'aventure se termine ici." << endl;
			return 0;
		}

		break;
	default:
		cout << "Ne rien faire ou essayer de tricher n'est pas la solution ! Vous etes vire du jeu." << endl;
		break;
	}

	cout << "Vous avez vaincu le troll de la foret ! Les villageois peuvent dormir tranquilles." << endl;


	system("PAUSE");
	return 0;
}