#ifndef GESTIONCOMBAT_H_FUNCTION
#define GESTIONCOMBAT_H_FUNCTION

#include<string>

int gestionCombat(int& pvHero, int& pvCreature, int const attHero, int const attCreature, int const deffHero, int const defCreature, std::string nomCreature);

#endif GESTIONCOMBAT_H_FUNCTION