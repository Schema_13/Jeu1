#include<iostream>
#include<string>
#include<stdlib.h>
#include<time.h>

using namespace std;

int gestionCombat(int& pvHero, int& pvCreature, int const attHero, int const attCreature, int const deffHero, int const defCreature, string const nomCreature)
{
	int pvMoins(0);

	while (pvCreature > 0 || pvHero > 0)
	{
		srand(time(NULL));
		// string varTmp("");
		int resultatDe(0);
		cout << "Vous lancez un de a 100 faces" << endl;
		cout << "Appuyez sur entree pour voir le resultat" << endl;
		cin.ignore();
		resultatDe = rand() % 100 + 1;
		cout << "Le resultat est " << resultatDe << endl;

		pvMoins = (resultatDe + (attHero - defCreature)) / 10;
		cout << nomCreature << " perd " << pvMoins << " points de vie." << endl;
		pvCreature -= pvMoins;
		pvMoins = 0;

		if (pvCreature >= 0)
		{
			cout << nomCreature << " vous attaque a son tour" << endl;
			resultatDe = rand() % 100 + 1;
			pvMoins = (resultatDe + (attCreature - deffHero)) / 10;
			pvHero -= pvMoins;
			cout << "Vous perdez " << pvMoins << " points de vie." << endl;
			pvMoins = 0;
		}
		else 
		{
			cout << "Un bon coup du tranchant et " << nomCreature << " est a terre" << endl;
			return 0;
		}

		if (pvHero <= 0)
		{
			return 1;
		}

	}

	return 0;
}